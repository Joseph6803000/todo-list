"use strict";

Template.form.events({
    'submit .add-new-task': function (event) {
        event.preventDefault();

        var taskName = event.currentTarget.children[0].firstElementChild.value;
        Collections.Todo.insert({
            name: taskName,
            createdAt: new Date(),
            complete: false
        });

        event.currentTarget.children[0].firstElementChild.value = "";
        return false;
    }
});

Template.todos.events({
    'click .delete-task': function (event) {
        Collections.Todo.remove({
            _id: this._id
        });
    },
    'click .complete-task': function (event) {
        Collections.Todo.update({
            _id: this._id
        }, {
            $set: {
                complete: true
            }
        });
    },

    'click .incomplete-task': function (event) {
        Collections.Todo.update({
            _id: this._id
        }, {
            $set: {
                complete: false
            }
        });
    }

});



Meteor.startup(function () {

    function setServerTime() {

        //get server time (it's in milliseconds)
        Meteor.call("getServerTime", function (error, result) {

            //get client time in milliseconds
            localTime = new Date().getTime();

            //difference between server and client
            var serverOffset = result - localTime;

            //store difference in the session
            Session.set("serverTimeOffset", serverOffset);

        });
    }

    function setDisplayTime() {
        var offset = Session.get("serverTimeOffset");
        var adjustedLocal = new Date().getTime() + offset;
        Session.set("serverTime", adjustedLocal);
    }

    //run these once on client start so we don't have to wait for setInterval
    setServerTime();
    setDisplayTime();

    //check server time every 15min
    setInterval(function updateServerTime() {
        setServerTime();
    }, 900000);

    //update clock on screen every second
    setInterval(function updateDisplayTime() {
        setDisplayTime();
    }, 1000);

});

//pass the clock to the HTML template
Template.register.clock = function () {
    return new Date(Session.get("serverTime"));
};